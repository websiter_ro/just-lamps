<?php
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 13.06.2018
	 * Time: 13:44
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );
	global $jl_order, $JL_PLUGIN;
	if ( $_REQUEST ) {
		$r = shortcode_atts( [
			'user_name'     => null,
			'user_email'    => null,
			'user_phone'    => null,
			'shipping_co'   => null,
			'shipping_code' => null,
			'billing_co'    => null,
			'billing_code'  => null,
			'consent'       => null
		], $_REQUEST );
		extract( $r );
		$errors = [];
		foreach ( $r as $key => $value ) {
			if ( $value ) {
				$jl_order[ $key ] = sanitize_text_field( $value );
			}
		}

		if ( $user_email && ! filter_var( $user_email, FILTER_VALIDATE_EMAIL ) ) {
			$errors[] = (object) [
				'user_email' => 'Email address is invalid.'
			];
		}
		if ( $user_name && $user_name !== sanitize_text_field( $user_name ) ) {
			$errors[] = (object) [
				'user_name' => 'Name field contains invalid characters.'
			];
		}

		if ( $user_email && filter_var( $user_email, FILTER_VALIDATE_EMAIL ) ) {
			$saved_order = $JL_PLUGIN->orders->get_order( $jl_order['id'] );
			if ($user_email && !$saved_order['email_sent']) {

			    $JL_PLUGIN->public->send_admin_email($jl_order);
				$JL_PLUGIN->public->send_email( $jl_order );

				$jl_order['email_sent'] = 1;
            }
		}

		$JL_PLUGIN->orders->update_order( $jl_order );
	}

?>
<div class="jl-order-page">
    <div>
        <div class="row">
            <div class="col-xs-12"><h2 class="post-title text-align-center">ORDER:
                    <code><?= str_pad( $jl_order['id'], 10, "0", STR_PAD_LEFT ); ?></code></h2></div>
            <div class="col-sm-4 user-details">
                <div>
                    <span class="label label-default">Name:</span>
                    <span data-prop="user_name"><?= $jl_order['user_name']; ?></span>
                </div>
                <div>
                    <span class="label label-default">Email:</span>
                    <span data-prop="user_email"><?= $jl_order['user_email']; ?></span>
                </div>
                <div>
                    <span class="label label-default">Phone:</span>
                    <span data-prop="user_phone"><?= $jl_order['user_phone']; ?></span>
                </div>
                <div>
                    <span class="label label-default">Shipping:</span>
                    <span data-prop="shipping_co"><?= $jl_order['shipping_co']; ?></span>,
                    <span data-prop="shipping_code"><?= $jl_order['shipping_code']; ?></span>
                </div>
                <div class="billing <?= $jl_order['billing_co'] ? '' : 'hidden'; ?>">
                    <span class="label label-default">Billing:</span>
                    <span data-prop="billing_co"><?= $jl_order['billing_co']; ?></span>,
                    <span data-prop="billing_code"><?= $jl_order['billing_code']; ?></span>
                </div>
                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userDetails">Edit
                    details
                </button>
                <script type="application/javascript">
									window.ajaxUrl = '<?= admin_url( 'admin-ajax.php' ); ?>';
									window.Invoice = {
										user: [{
											name: 'user_name',
											label: 'Full name',
											value: '<?= $jl_order['user_name'];?>'
										}, {
											name: 'user_email',
											label: 'Email',
											value: '<?= $jl_order['user_email'];?>'
										}, {
											name: 'user_phone',
											label: 'Phone',
											value: '<?= $jl_order['user_phone'];?>'
										}, {
											name: 'shipping_co',
											label: 'Shipping Address',
											value: '<?= $jl_order['shipping_co'];?>'
										}, {
											name: 'shipping_code',
											label: 'Shipping Eircode',
											value: '<?= $jl_order['shipping_code'];?>'
										}, {
											name: 'billing_co',
											label: 'Billing Address',
											value: '<?= $jl_order['billing_co'];?>'
										}, {
											name: 'billing_code',
											label: 'Billing Eircode',
											value: '<?= $jl_order['billing_code'];?>'
										}],
										tableItems: '<?= $jl_order['invoice_table'];?>',
										varRef: '<?= $jl_order['var_ref'];?>'
									};
                </script>
            </div>
            <div class="payment-ribbon">
                <div class="payment-status status-<?= $jl_order['status']; ?>">
                    <strong><?= $JL_PLUGIN->orders->get_status_message( $jl_order['status'] ); ?></strong>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="invoiceContent" class="table-responsive invoice-table"></div>
            </div>
        </div>
    </div>
    <div class="loader-jl"><div class="loader-5 center"><span></span></div></div>
    <div class="modal" tabindex="-1" role="dialog" id="userDetails">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update invoice details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
					<?php include( __DIR__ . "/invoice_modal.php" ); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary submit-user-details">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
