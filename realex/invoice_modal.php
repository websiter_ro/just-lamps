<?php
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 21/06/18
 * Time: 07:25
 */

global $jl_order;
?>
    <form class="client-details-form">
        <div class="row">
            <div class="col-sm-6">
                <fieldset class="f-group">
                    <legend>Shipping / Billing</legend>
                    <div class="form-group row">
                        <label for="user_name" class="col-sm-4">Full name</label>
                        <div class="col-sm-8">
                            <input type="text" id="user_name" name="user_name" class="form-control" value="<?= $jl_order['user_name']; ?>"
                                   tabindex="-1"
                            >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="user_email" class="col-sm-4">Email</label>
                        <div class="col-sm-8">
                            <input type="text" id="user_email" name="user_email" class="form-control"
                                   value="<?= $jl_order['user_email']; ?>"
                                   tabindex="-1"
                            >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="user_phone" class="col-sm-4">Phone</label>
                        <div class="col-sm-8">
                            <input type="text" id="user_phone" name="user_phone" class="form-control"
                                   value="<?= $jl_order['user_phone']; ?>"
                                   tabindex="-1"
                            >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="shipping_co" class="col-sm-4">Shipping Address</label>
                        <div class="col-sm-8">
                            <textarea id="shipping_co" name="shipping_co" class="form-control" tabindex="-1"
                            ><?= $jl_order['shipping_co']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="shipping_code" class="col-sm-4">Shipping Eircode</label>
                        <div class="col-sm-8">
                            <input type="text" id="shipping_code" name="shipping_code" class="form-control" value="<?= $jl_order['shipping_code']; ?>">
                        </div>
                    </div>
                    <input type="checkbox" id="use_shipping" name="use_shipping" <?= $jl_order['billing_co'] ? '': 'checked';?>>
                    <label for="use_shipping" class="jl-check">Use same address for billing.</label>
                    <div class="billing">
                        <div class="form-group row">
                            <label for="billing_co" class="col-sm-4">Billing Address</label>
                            <div class="col-sm-8">
                                <textarea id="billing_co" name="billing_co" class="form-control" tabindex="-1"
                                ><?= $jl_order['billing_co']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="billing_code" class="col-sm-4">Billing Eircode</label>
                            <div class="col-sm-8">
                                <input type="text" id="billing_code" name="billing_code" class="form-control"
                                       value="<?= $jl_order['billing_code']; ?>"
                                       tabindex="-1"
                                >
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-sm-6 inverted">
                <fieldset class="f-group">
                    <legend>Personal data</legend>
                    <div class="row">
                        <div class="col-xs-12">
                            <p>To have the goods delivered, please provide <strong>correct</strong> information.</p>
                            <p>Your personal details will not be shared with any third party and will only be used for delivery
                                of purchased goods.</p>
                            <p>You can <strong>anonymize</strong> your order at any time, from invoice page.</p>
                        </div>
                    </div>
                    <input type="checkbox" name="consent" id="consent" <?= $jl_order['user_name'] || $jl_order['user_phone'] || $jl_order['user_email'] ? 'checked' : '';?>>
                    <label for="consent" class="jl-check">You agree to our <a
                                href="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/privacy-policy/'; ?>" title="Our privacy policy. (= Your data is safe and we do not spam)">terms
                            and conditions</a> and grant permission for your personal data to be stored with this order.</label>
                </fieldset>
                <fieldset class="f-group">
                    <legend>Resend emails</legend>
                    <input type="checkbox" name="resend_email" id="resend_email">
                    <label for="resend_email" class="jl-check">Check to resend confirmation email.</label>
                </fieldset>

            </div>
        </div>
    </form>

