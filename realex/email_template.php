<?php
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 10.08.2018
	 * Time: 20:17
	 */
	global $jl_order;
	echo '<h2>ORDER CONFIRMATION</h2><p>Your order <span style="color:#ff5500;font-weight: bold;">#' .
	     str_pad( $jl_order['id'], 10, "0", STR_PAD_LEFT ) . '</span></p><hr>'.
	     '<p>'.implode(' ', array_merge(['Dear'],explode(" ",$jl_order['user_name']))).',</p>'.
	     '<p>Your order has been received. <br>If you have any questions do not hesitate to contact us. </p>';
	$hSt1 = "box-sizing:border-box;background:0px 0px;border:0px;margin:0px;padding:0px;vertical-align:baseline;outline:0px";
	$hSt2 = "box-sizing:border-box;padding:8px;background:0px 0px;border-width:1px 0px 0px;border-top-style:solid;border-right-style:initial;border-bottom-style:initial;border-left-style:initial;border-top-color:rgb(221,221,221);border-right-color:initial;border-bottom-color:initial;border-left-color:initial;vertical-align:top;outline:0px;line-height:1.42857";
	$hSt3 = "box-sizing:border-box;background:0px 0px;border-width:0px 0px 2px;border-top-style:initial;border-right-style:initial;border-bottom-style:solid;border-left-style:initial;border-top-color:initial;border-right-color:initial;border-bottom-color:rgb(221,221,221);border-left-color:initial;margin:0px;vertical-align:bottom;outline:0px;line-height:1.42857;padding:8px";
	$tbl_ = '<table style="border:10px solid #282a72 !important;box-sizing:border-box;border-collapse:collapse;background:0px 0px;margin:0px 0px 20px;padding:0px;vertical-align:middle;outline:0px;width:100%;max-width:660px">';
	$tblE = '</table>';
	$thd_ = "<thead style='${$hSt1}'>";
	$thdE = '</thead>';
	$tbd_ = "<tbody style='${$hSt1}'>";
	$tbdE = '</tbody>';
	$tft_ = "<tfoot style='${hSt1}'>";
	$tftE = '</tfoot>';
	$tr_  = "<tr style='${hSt1}'>";
	$tr_b = "<tr style='${hSt1};font-weight:bold;background-color: #f5f5f5'>";
	$trE  = '</tr>';
	$th_  = "<th style='{$hSt3};text-align:center'>";
	$th_l  = "<th style='{$hSt3};text-align:left'>";
	$th_r  = "<th style='{$hSt3};text-align:right'>";
	$thE  = '</th>';
	$td_  = "<td style='${hSt2};text-align:center'>";
	$td_l   = "<td style='${hSt2};text-align:left'>";
	$td_r   = "<td style='${hSt2};text-align:right'>";
	$tdE    = '</td>';
	$out    = $tbl_ . $thd_ . $tr_b .
	          $th_ . "No." . $thE .
	          $th_l . "Product" . $thE .
	          $th_ . "Qty." . $thE .
	          $th_r . "Price" . $thE .
	          $th_r . "Total" . $thE .
	          $trE . $thdE . $tbd_;
	$iTable = json_decode( $jl_order['invoice_table'] );
	if ( is_array( $iTable ) || is_object( $iTable ) ) {
		foreach ( $iTable as $key => $row ) {
			$isLast = $key === count( $iTable ) - 1;
			if ( $key > 0 ) {
				$out .= ( $isLast ? $tbdE . $tft_ . $tr_b : $tr_ ) .
				        $td_ . $row[0] . $tdE .
				        $td_l . $row[1] . $tdE .
				        $td_ . $row[2] . $tdE .
				        $td_r . $row[3] . $tdE .
				        $td_r . $row[4] . $tdE .
				        $trE;
			}
		}
	}
	$out .= $tftE . $tblE;
	$out .= '<hr><p>Thank you for shopping with toomeyav.ie</p>';

	echo $out;
