<?php
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );
// check if orders table exists
	global $wpdb;
	$table_name   = $wpdb->prefix . 'justlamps_orders';
	$settings     = get_option( 'jlc-settings' );
	if (!isset($settings['realex_url'])) {
		$settings['realex_url'] = 'sandbox';
	}
	$response_uri = plugin_dir_url( __FILE__ );

	$products = json_decode( json_encode( $_REQUEST['products'] ) );
	$ids      = '';
	$amount   = 0;
	$vat_rate = isset($settings['vat_rate']) ? $settings['vat_rate'] /100 : .23;

	$invoice_table = [
		[ 'No.', 'Product', 'Qty.', 'Price', 'Total' ]
	];

	$comment = '';
	$index   = 0;

	$flat_rate = 8000;
	foreach ( $products as $product ) {
		$price           = floatval( $product->{'11'} ) * 100 + $flat_rate;
		$qty             = floatval( $product->{'qty'} );
		$ids             .= $product->{'0'} . ',';
		$invoice_table[] = [
			++ $index,
			"{$product->{'1'}} - {$product->{'2'}} <br>{$product->{'3'}} - {$product->{'4'}} <br>{$product->{'6'}}",
			$qty,
			"€" . number_format( $price / 100, 2 ),
			"€" . number_format( $price * $qty / 100, 2 )
		];
		$comment         .= "{$product->{'0'}}--{$product->{'6'}} \n";
		$amount          += $qty * $price;
	}
	$ids = rtrim( trim( $ids ), ',' );

	$invoice_table[] = [
		null,
		null,
		"Total:",
		null,
		"€" . number_format( $amount / 100, 2 )
	];
	$invoice_table[] = [
		null,
		null,
		"V.A.T.:",
		null,
		"€" . number_format( $amount / 100 * $vat_rate, 2 )
	];
	$invoice_table[] = [
		null,
		null,
		"Grand Total:",
		null,
		"€" . number_format( $amount / 100 * ($vat_rate + 1), 2 )
	];
	$comment         .=
		"     Total: " . number_format( $amount / 100, 2 ) . "\n" .
		"    V.A.T.:  " . number_format( $amount / 100 * $vat_rate, 2 ) . "\n_________________________\n" .
		"Grand total: " . number_format( $amount / 100 * ($vat_rate + 1), 2 );

	$r = (object) [
		'TIMESTAMP'             => date_format( new DateTime(), 'YmdHis' ),
		'MERCHANT_ID'           => $settings['account_name'],
		'ACCOUNT'               => 'internet',
		'AUTO_SETTLE_FLAG'      => '1',
		'CHANNEL'               => 'ECOM',
		'CURRENCY'              => isset($settings['currency']) ? $settings['currency'] : 'EUR',
		'AMOUNT'                => round( $amount * ($vat_rate + 1), 0, PHP_ROUND_HALF_UP ),
		'COMMENT1'              => $comment,
		'COMMENT2'              => null,
		'PROD_ID'               => $ids,
		'HPP_LANG'              => 'EN',
		'HPP_VERSION'           => '2',
		'MERCHANT_RESPONSE_URL' => $response_uri,
		'CARD_PAYMENT_BUTTON'   => 'Pay Now'
	];

	$jl_order = $wpdb->insert(
		$table_name,
		array(
			'created_at'    => current_time( 'mysql' ),
			'timestamp'     => $r->TIMESTAMP,
			'amount'        => $r->AMOUNT,
			'comment1'      => $r->COMMENT1,
			'invoice_table' => json_encode( $invoice_table )
		)
	);
	if ( $jl_order ) {
		$order_id = $wpdb->insert_id;
	}

	$r->ORDER_ID = "TST-" . str_pad( $order_id, 10, "0", STR_PAD_LEFT );
	$r->VAR_REF  = "Invoice " . $r->ORDER_ID;
	$r->action = $settings['realex_url'];

	$r->SHA1HASH = sha1( sha1( $r->TIMESTAMP . '.' . $r->MERCHANT_ID . '.' . $r->ORDER_ID . '.' . $r->AMOUNT . '.' . $r->CURRENCY ) . '.' . 'secret' );
	echo json_encode( $r );
