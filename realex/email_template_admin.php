<?php
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 10.08.2018
	 * Time: 20:17
	 */
	global $jl_order;
	echo '<h2>NEW ORDER: <span style="color:#ff5500;font-weight: bold;">#' .
	     str_pad( $jl_order['id'], 10, "0", STR_PAD_LEFT ) . '</span></h2>';
	$invoice_link = null;
	$headSt = "style='width:130px; display: inline-block'";
	$thSt = "box-sizing:border-box;padding:8px;background:0px 0px;border-width:0px 0px 2px;border-top-style:initial;border-right-style:initial;border-bottom-style:solid;border-left-style:initial;border-top-color:initial;border-right-color:initial;border-bottom-color:rgb(221,221,221);border-left-color:initial;margin:0px;vertical-align:bottom;outline:0px;line-height:1.42857";
	$bodySt = "style='box-sizing:border-box;background:0px 0px;border:0px;margin:0px;padding:0px;vertical-align:baseline;outline:0px'";
	$footSt = "box-sizing:border-box;padding:8px;background:0px 0px;border-width:1px 0px 0px;border-top-style:solid;border-right-style:initial;border-bottom-style:initial;border-left-style:initial;border-top-color:rgb(221,221,221);border-right-color:initial;border-bottom-color:initial;border-left-color:initial;vertical-align:top;outline:0px;line-height:1.42857";
	if (isset($jl_order['var_ref'])) {
		$uid          = json_decode( $jl_order['request'] )->pas_uuid;
		$id           = $jl_order['var_ref'];
		$invoice_link = get_site_url().'/order/' . $id . '/?uid=' . $uid;
	}

	if ($invoice_link) {
		echo "<p>[<a href='{$invoice_link}'>View invoice</a>]</p>";
	}
	echo "<div><strong {$headSt}>Name:</strong>" . $jl_order['user_name'] . "</div>" .
	     "<div><strong {$headSt}>Phone:</strong>" . $jl_order['user_phone'] . "</div>" .
	     "<div><strong {$headSt}>Email:</strong>" . $jl_order['user_email'] . "</div>" .
	     "<div><strong {$headSt}>Address:</strong>" . $jl_order['shipping_co'] . "</div>" .
	     "<div><strong {$headSt}>Eircode:</strong>" . $jl_order['shipping_code'] . "</div>" .
	     "<hr>";
	$tbl_   = '<table style="border: 10px solid #282a72 !important;box-sizing:border-box;border-collapse:collapse;background:0px 0px;margin:0px 0px 20px;padding:0px;vertical-align:middle;outline:0px;width:100%;max-width:660px">';
	$tblE   = "</table>";
	$thd_   = "<thead {$bodySt}>";
	$thdE   = "</thead>";
	$tbd_   = "<tbody {$bodySt}>";
	$tbdE   = "</tbody>";
	$tft_   = "<tfoot {$bodySt}>";
	$tftE   = "</tfoot>";
	$tr_    = "<tr {$bodySt}>";
	$tr_b   = "<tr style='box-sizing:border-box;background:0px 0px;border:0px;margin:0px;padding:0px;vertical-align:baseline;outline:0px;font-weight:bold;background-color: #f5f5f5;'>";
	$trE    = "</tr>";
	$th_    = "<th style='{$thSt};text-align:center'>";
	$th_l   = "<th style='{$thSt};text-align:left'>";
	$th_r   = "<th style='{$thSt};text-align:right'>";
	$thE    = "</th>";
	$td_    = "<td style='{$footSt};text-align:center'>";
	$td_l   = "<td style='{$footSt};text-align:left'>";
	$td_r   = "<td style='{$footSt};text-align:right;'>";
	$tdE    = "</td>";
	$out    = $tbl_ . $thd_ . $tr_b .
	          $th_ . "No." . $thE .
	          $th_l . "Product" . $thE .
	          $th_ . "Qty." . $thE .
	          $th_r . "Price" . $thE .
	          $th_r . "Total" . $thE .
	          $trE . $thdE . $tbd_;
	$iTable = json_decode( $jl_order['invoice_table'] );
	if ( is_array( $iTable ) || is_object( $iTable ) ) {
		foreach ( $iTable as $key => $row ) {
			$isLast = $key === count( $iTable ) - 1;
			if ( $key > 0 ) {
				$out .= ( $isLast ? $tbdE . $tft_ . $tr_b : $tr_ ) .
				        $td_ . $row[0] . $tdE .
				        $td_l . $row[1] . $tdE .
				        $td_ . $row[2] . $tdE .
				        $td_r . $row[3] . $tdE .
				        $td_r . $row[4] . $tdE .
				        $trE;
			}
		}
	}
	$out .= $tftE . $tblE;

	echo $out;
