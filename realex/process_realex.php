<?php
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 10.06.2018
	 * Time: 17:01
	 */
	global $JL_PLUGIN;
	$args = array(
		'RESULT'                => null,
		'AUTHCODE'              => null,
		'MESSAGE'               => null,
		'PASREF'                => null,
		'AVSPOSTCODERESULT'     => null,
		'AVSADDRESSRESULT'      => null,
		'CVNRESULT'             => null,
		'ACCOUNT'               => null,
		'MERCHANT_ID'           => null,
		'ORDER_ID'              => null,
		'TIMESTAMP'             => null,
		'AMOUNT'                => null,
		'CARD_PAYMENT_BUTTON'   => null,
		'MERCHANT_RESPONSE_URL' => null,
		'HPP_LANG'              => null,
		'SHIPPING_CODE'         => null,
		'SHIPPING_CO'           => null,
		'BILLING_CODE'          => null,
		'BILLING_CO'            => null,
		'COMMENT1'              => null,
		'COMMENT2'              => null,
		'ECI'                   => null,
		'CAVV'                  => null,
		'XID'                   => null,
		'SHA1HASH'              => null,
		'pas_uuid'              => null,
		'user_name'             => null,
		'user_email'            => null,
		'user_phone'            => null,

	);
	extract( $args );
	extract( shortcode_atts( $args, $_REQUEST ) );
	$JL_PLUGIN->log( $_REQUEST );
?>
    <form action="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/order/' . $PASREF . '/?uid=' . $pas_uuid; ?>"
          method="post" class="order_form">
        <fieldset class="f-group">
            <legend>Shipping / Billing</legend>
            <div class="form-group">
                <label for="user_name">Full name</label>
                <input type="text" id="user_name" name="user_name" class="form-control" value="<?= $user_name; ?>"
                       tabindex="-1"
                >
            </div>
            <div class="form-group">
                <label for="user_email">Email</label>
                <input type="text" id="user_email" name="user_email" class="form-control"
                       value="<?= $user_email; ?>"
                       tabindex="-1"
                >
            </div>
            <div class="form-group">
                <label for="user_phone">Phone</label>
                <input type="text" id="user_phone" name="user_phone" class="form-control"
                       value="<?= $user_phone; ?>"
                       tabindex="-1"
                >
            </div>
            <div class="form-group">
                <label for="shipping_co">Shipping Address</label>
                <textarea type="text" id="shipping_co" name="shipping_co" class="form-control"
                ><?= trim($SHIPPING_CO) ?></textarea>
            </div>
            <div class="form-group">
                <label for="shipping_code">Shipping Eircode</label>
                <input type="text" id="shipping_code" name="shipping_code" class="form-control"
                       value="<?= $SHIPPING_CODE ?>"
                       tabindex="-1"
                >
            </div>
            <input type="checkbox" id="use_shipping" name="use_shipping" checked class="hidden">
            <label for="use_shipping" class="jl-check">Use same address for billing.</label>
            <div class="billing">
                <div class="form-group">
                    <label for="billing_co">Billing Address</label>
                    <textarea type="text" id="billing_co" name="billing_co" class="form-control" tabindex="-1"
                    ><?= trim($BILLING_CO) ?></textarea>
                </div>
                <div class="form-group">
                    <label for="billing_code">Billing Eircode</label>
                    <input type="text" id="billing_code" name="billing_code" class="form-control"
                           value="<?= $BILLING_CODE ?>"
                           tabindex="-1"
                    >
                </div>
            </div>
        </fieldset>
        <input type="checkbox" name="consent" id="consent" class="hidden">
        <fieldset class="f-group">
            <legend>Personal data</legend>
            <div class="row">
                <div class="col-xs-12">
                    <p>To have the goods delivered, please provide <strong class="orange">correct</strong> information.
                    </p>
                    <p>Your personal details will not be shared with any third party and will only be used for delivery
                        of purchased goods.</p>
                    <p>You can <strong>anonymize</strong> your order at any time, from the invoice page.</p>
                </div>
            </div>
            <label for="consent" class="jl-check">You agree to our <a
                        href="<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/privacy-policy/'; ?>" title="Our privacy policy. (Your data is safe and we do not spam)">terms
                    and
                    conditions</a> and grant permission for your personal data to be stored with this order.</label>
        </fieldset>
        <button type="submit" class="btn btn-lg btn-primary btn-block save-order-details">Save order
            details
        </button>
        <button type="submit" class="btn btn-lg btn-primary btn-block save-order-details" disabled="disabled">Save order
            details
        </button>
    </form>
<?php
