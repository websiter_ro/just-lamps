<?php
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 06.06.2018
	 * Time: 03:24
	 */

	require_once( "../../../../wp-load.php" );
	global $JL_PLUGIN;

?><!doctype html>
    <html>
    <head>
        <link rel="stylesheet" href="<?= $JL_PLUGIN->plugin_dir_url . 'public/css/just-lamps-public.css?v=' . time(); ?>">
    </head>
    <body>
	<?php
		$r = $_REQUEST;

		$error_html = '<style>body {
            background-color: #f5f5f5;
        }</style>
    <div style="top: 100px; width: 600px; max-width: 90vw; margin: 100px auto; padding: 15px; border-radius: 4px; box-shadow: 0 3px 5px -1px rgba(0,0,0,.1), 0 5px 8px 0 rgba(0,0,0,.07), 0 1px 14px 0 rgba(0,0,0,.06);font-family: sans-serif; background-color: #fff">
        <p>Something went wrong... Please <a href="javascript:window.history.go(-1);"><strong>try again</strong></a>.
    </div>';

		if ( $r ) {

			$settings = get_option( 'jlc-settings' );

			if ( sha1( sha1( $r['TIMESTAMP'] . '.' . $r['MERCHANT_ID'] . '.' .
			                 $r['ORDER_ID'] . '.' . $r['RESULT'] . '.' .
			                 $r['MESSAGE'] . '.' . $r['PASREF'] . '.' .
			                 $r['AUTHCODE'] ) . '.' . $settings['shared'] ) === $r['SHA1HASH'] ) {

				global $JL_PLUGIN;
				$order_id = (int) substr( $r['ORDER_ID'], - 10 );
				$jl_order    = $JL_PLUGIN->orders->get_order( $order_id );

				if ( $r['RESULT'] === "00" ) {
					$out = '<div class="order-received panel panel-success">
    <h2 class="panel-heading">Payment successful</h2>
    <div class="panel-body">
        <p>Payment confirmation received. Please enter your details below to ensure swift processing of your order.</p>
</div></div>';
				} else {
					$out = '<div class="order-received panel panel-warning"><h2 class="panel-heading">Error processing payment</h2><div class="panel-body">
<p class="lead">Error code: <code>' . $r['RESULT'] . '</code></p>
<p>' . $JL_PLUGIN->orders->get_status_message( $r['RESULT'] ) . '</p>
<p><a href="javascript:window.history.go(-1);"><strong>&laquo; Go back</strong></a></p>
</div></div>';
				}
				$jl_order['status']  = $r['RESULT'];
				$jl_order['var_ref'] = $r['PASREF'];
				$jl_order['request'] = json_encode( $r );
				$jl_order['consent'] = 0;

				$JL_PLUGIN->orders->update_order( $jl_order );

				echo $out;

				if ( $r['RESULT'] === "00" ) {

					include( 'process_realex.php' );
				}
			} else {
				echo $error_html;
			}
		} else {
			echo $error_html;
		}
	?>
    </body>
    </html>
<?php
