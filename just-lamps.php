<?php
	/**
	 * The plugin bootstrap file
	 *
	 * This file is read by WordPress to generate the plugin information in the plugin
	 * admin area. This file also includes all of the dependencies used by the plugin,
	 * registers the activation and deactivation functions, and defines a function
	 * that starts the plugin.
	 *
	 * @link              http://websiter.ro
	 * @since             1.0.0
	 * @package           Just_Lamps
	 *
	 * @wordpress-plugin
	 * Plugin Name:       Just Lamps Client
	 * Plugin URI:        http://websiter.ro
	 * Description:       Simple plugin for fetching data from justlamps.net.
	 * Version:           1.0.0
	 * Author:            Andrei Gheorghiu
	 * Author URI:        http://websiter.ro
	 * License:           GPL-2.0+
	 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
	 * Text Domain:       just-lamps
	 * Domain Path:       /languages
	 */

// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	/**
	 * Currently plugin version.
	 * Start at version 1.0.0 and use SemVer - https://semver.org
	 * Rename this for your plugin and update it as you release new versions.
	 */
	define( 'JUST_LAMPS_VERSION', '1.0.1' );

	/**
	 * The code that runs during plugin activation.
	 * This action is documented in includes/class-just-lamps-activator.php
	 */
	function activate_just_lamps() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-just-lamps-activator.php';
		Just_Lamps_Activator::get_instance();
		Just_Lamps_Activator::update_columns();
	}

	/**
	 * The code that runs during plugin deactivation.
	 * This action is documented in includes/class-just-lamps-deactivator.php
	 */
	function deactivate_just_lamps() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-just-lamps-deactivator.php';
		Just_Lamps_Deactivator::deactivate();
	}

	register_activation_hook( __FILE__, 'activate_just_lamps' );
	register_deactivation_hook( __FILE__, 'deactivate_just_lamps' );

	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	 */
	require plugin_dir_path( __FILE__ ) . 'includes/class-just-lamps.php';

	/**
	 * Begins execution of the plugin.
	 *
	 * Since everything within the plugin is registered via hooks,
	 * then kicking off the plugin from this point in the file does
	 * not affect the page life cycle.
	 *
	 * @since    1.0.0
	 */
	function run_just_lamps() {

		global $JL_PLUGIN;
		if ( ! $JL_PLUGIN ) {
			$JL_PLUGIN                 = new Just_Lamps();
			$JL_PLUGIN->plugin_dir_url = plugin_dir_url( __FILE__ );
		}
		activate_just_lamps();
		$JL_PLUGIN->run();

	}

	run_just_lamps();
