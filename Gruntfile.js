module.exports = function (grunt) {
	grunt.initConfig({
		postcss: {
			options: {
				processors: [
					require('pixrem'),
					require('autoprefixer')({browsers: ['> 0%']}),
					require('postcss-flexboxfixer'),
					require('cssnano')({autoprefixer: false, zindex: false})
				]
			},
			run: {
				files: [{
					expand: true,
					flatten: true,
					src: ['**/scss/*.css', '!**/_vars.css'],
					dest: 'public/css'
				}]
			}
		},
		sass: {
			dist: {
				files: [{
					expand: true,
					src: ['**/scss/*.scss'],
					ext: ".css"
				}]
			}
		},
		watch: {
			styles: {
				files: [
					'/**/scss/*.css'
				],
				tasks: ['sass', 'postcss:run']
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ["watch:styles"]);
	grunt.registerTask('css', ['sass', 'postcss:run']);
};