<?php

	/**
	 * The public-facing functionality of the plugin.
	 *
	 * @link       http://websiter.ro
	 * @since      1.0.0
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/public
	 */

	/**
	 * The public-facing functionality of the plugin.
	 *
	 * Defines the plugin name, version, and two examples hooks for how to
	 * enqueue the public-facing stylesheet and JavaScript.
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/public
	 * @author     Andrei Gheorghiu <mail@websiter.ro>
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	class Just_Lamps_Public {

		/**
		 * Unique identifier
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string $plugin_name The ID of this plugin.
		 */
		private $plugin_name;

		/**
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string $version The current version of this plugin.
		 */
		private $version;

		/**
		 * Init.
		 *
		 * @since    1.0.0
		 *
		 * @param      string $plugin_name The name of the plugin.
		 * @param      string $version The version of this plugin.
		 */
		public function __construct( $plugin_name, $version ) {

			$this->plugin_name = $plugin_name;
			$this->version     = $version;

		}

		/**
		 * Register css for public-facing side.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_styles() {

			wp_enqueue_style( 'jquery-ui-custom', plugin_dir_url( __DIR__ ) . 'vendor/jquery-ui/jquery-ui.theme.min.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/just-lamps-public.css', array(), time(), 'all' );
			wp_enqueue_style( 'checkout-cornerflat', plugin_dir_url( __FILE__ ) . 'css/checkout-cornerflat.css', array(), $this->version, 'all' );
			global $post;
			if ( is_object( $post ) && $post->ID == get_option( 'jlc-settings' )['order_page'] ) {
				wp_enqueue_style( 'twitter-bootstrap', plugin_dir_url( __DIR__ ) . 'vendor/twitter-bootstrap/bootstrap.min.css', array(), $this->version, 'all' );
			}

		}

		/**
		 * Register js for the public-facing side.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_scripts() {

			global $post;
			if ( is_object( $post ) && has_shortcode( $post->post_content, 'just-lamps' ) ) {
				wp_dequeue_script( "smoothPageScroll" );
			}
			wp_enqueue_script( 'custom-lodash', plugin_dir_url( __DIR__ ) . 'vendor/lodash/lodash.min.js', array(), time(), false );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/just-lamps-public.js', array(
				'jquery',
				'custom-lodash'
			), time(), false );
			if ( is_object( $post ) && $post->ID == get_option( 'jlc-settings' )['order_page'] ) {
				wp_enqueue_script( 'twitter-bootstrap', plugin_dir_url( __DIR__ ) . 'vendor/twitter-bootstrap/bootstrap.min.js', array( 'jquery' ), time(), false );
				wp_enqueue_script( 'invoice', plugin_dir_url( __FILE__ ) . 'js/invoice.js', array( 'jquery' ), time(), false );
			}
		}

		public function render() {
			ob_start();
			include 'partials/just-lamps-public-display.php';

			return do_shortcode( ob_get_clean() );
		}

		public function projector_form() {
			ob_start();
			include 'partials/just-lamps-order-projector.php';

			return do_shortcode( ob_get_clean() );
		}

		public function payment_json() {
			require( __DIR__ . '/../realex/realex.php' );
			die();
		}

		public function get_order( $id ) {
			global $JL_PLUGIN;

			return $JL_PLUGIN->orders->get_order( $id );
		}

		public function order_page() {
			$var_ref = array_filter( explode( '/', $_SERVER['REQUEST_URI'] ) )[2];
			if ( $var_ref ) {
				global $JL_PLUGIN, $jl_order;
				$jl_order = $JL_PLUGIN->orders->get_order_by_code( $var_ref );
				if ( $jl_order ) {
					$pas_uuid = json_decode( $jl_order['request'] )->pas_uuid;
					if ( $pas_uuid === $_REQUEST['uid'] ) {
						include( __DIR__. '/../realex/invoice.php' );

						return;
					}
				}
			}
			global $wp_query;
			$wp_query->set_404();

		}

		public function add_orders_permalink() {
			$settings = get_option( 'jlc-settings' );
			$page_id  = $settings['order_page'];
			if ( $page_id ) {
				add_rewrite_rule(
					'order/(([^/]+))?',
					'index.php?page_id=' . $page_id . '&order_code=$matches[1]',
					'top'
				);
				add_filter( 'query_vars', array( $this, 'order_vars' ) );
				add_action( 'pre_get_posts', array( $this, 'order_permalink' ), 10, 3 );
			}
		}

		public function order_vars( $query_vars ) {
			$query_vars[] = 'order_code';

			return $query_vars;
		}

		public function order_permalink( $query ) {

			$settings = get_option( 'jlc-settings' );
			$page_id  = $settings['order_page'];

			if ( isset( $query->query_vars['order_code'] )
			     && ! empty( $query->query_vars['order_code'] )
			     && strlen($query->query_vars['order_code']) === 17
			     && ! is_numeric( $query->query_vars['order_code'] )
			     && isset( $query->query_vars['page_id'] ) ) {

				switch ( $query->query_vars['page_id'] ) {
					case $page_id:
						global $JL_PLUGIN;
						$query->query_vars['order_code'] = $JL_PLUGIN->orders->get_order_by_code( $query->query_vars['order_code'] )['var_ref'];
						if ( empty( $query->query_vars['order_code'] ) ) {
							//404...
						}
						break;
					default:
						break;
				}
			}
		}

		public function send_admin_email($jl_order) {
			global $JL_PLUGIN, $jl_order;
			ob_start();

			include( __DIR__. "/../realex/email_template_admin.php");

			$message = ob_get_clean();

			$headers = 'Content-type: text/html;charset=utf-8' . "\r\n";
			$headers .= 'From: Toomeyav.ie <wordpress@toomeyav.ie>' . "\r\n";
			$subject = 'New order #' . str_pad( $jl_order['id'], 10, "0", STR_PAD_LEFT ) . "\r\n";
			wp_mail( $JL_PLUGIN->admin->get_option('admin_email'), $subject, $message, $headers);
		}

		public function send_email($jl_order) {
			global $jl_order;
			ob_start();

			include( __DIR__. "/../realex/email_template.php");

			$message = ob_get_clean();

			$headers = 'Content-type: text/html;charset=utf-8' . "\r\n";
			$headers .= 'From: Toomeyav.ie <wordpress@toomeyav.ie>' . "\r\n";
			$subject = 'Order confirmation #' . str_pad( $jl_order['id'], 10, "0", STR_PAD_LEFT ) . "\r\n";
			wp_mail( $jl_order['user_email'], $subject, $message, $headers);
		}

		public function invoice_user() {
			global $JL_PLUGIN, $jl_order;
			$jl_order      = $JL_PLUGIN->orders->get_order_by_code( $_REQUEST['ref'] );
			$consent    = $_REQUEST['user'] ? $_REQUEST['user']['consent'] === "true" : true;
			$resend     = $_REQUEST['user'] ? $_REQUEST['user']['resend_email'] === "true" : null;
			$user_email = ($consent || $resend) ? esc_html( esc_attr( $_REQUEST['user']['user_email'] ) ) : null;

			$jl_order['consent'] = $consent ? 1: 0;
			$jl_order['user_name']  = $consent ? esc_html( esc_attr( $_REQUEST['user']['user_name'] ) ) : null;
			$jl_order['user_phone'] = $consent ? esc_html( esc_attr( $_REQUEST['user']['user_phone'] ) ) : null;

			$jl_order['shipping_co']   = trim( esc_html( esc_attr( $_REQUEST['user']['shipping_co'] ) ) );
			$jl_order['shipping_code'] = esc_html( esc_attr( $_REQUEST['user']['shipping_code'] ) );
			$use_same               = $_REQUEST['user']['use_shipping'] === "true";
			$jl_order['billing_co']    = $use_same ? null : trim( esc_html( esc_attr( $_REQUEST['user']['billing_co'] ) ) );
			$jl_order['billing_code']  = $use_same ? null : esc_html( esc_attr( $_REQUEST['user']['billing_code'] ) );

			if (($user_email && ! $jl_order['email_sent']) || $resend) {
				$jl_order['user_email'] = $user_email;
				$this->send_admin_email($jl_order);
				$this->send_email($jl_order);
				$jl_order['email_sent'] = 1;
			} else {
				$jl_order['user_email'] = $user_email;
			}
			ob_start();
			$JL_PLUGIN->orders->update_order($jl_order);
			echo json_encode( $jl_order );
			die();
		}

	}
