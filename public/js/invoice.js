(function ($) {
	$(window).on('load', parseInvoice);
	function parseInvoice() {
		let Invoice = window.Invoice;
		_.extend(Invoice, {
			modal: $('#userDetails'),
			form: $('.client-details-form'),
			init: () => {
				Invoice.modal.appendTo('body');
				Invoice.tableItems = JSON.parse(Invoice.tableItems);
				Invoice.setTable(Invoice.tableItems);
				$('.loader-jl').fadeOut();
			},
			getUser: () => {
				let out = {};
				_.each(Invoice.user, user => {
					out[user.name] = Invoice.form[0].elements[user.name].value
				});
				out.consent = $('#consent').is(':checked');
				out.use_shipping = $('#use_shipping').is(':checked');
				out.resend_email = $('#resend_email').is(':checked');
				
				return out;
			},
			setTable: items => {
				let tAble = $('<table />', {
						class: 'table'
					}),
					tHead = $('<thead />'),
					tBody = $('<tbody />'),
					tFoot = $('<tfoot />');
				_.each(items, (o, k) => Invoice.appendRow(k ? o[0] ? tBody : tFoot : tHead, k ? 'td' : 'th', o));
				tAble.append(tHead).append(tBody).append(tFoot);
				$('#invoiceContent').html(tAble.prop('outerHTML'));
			},
			appendRow: (parent, tag, item) => {
				let row = $('<tr />');
				_.each(item, html => row.append($('<' + tag + ' />', {
					html
				})));
				parent.append(row);
			}
		});
		if (Invoice) {
			Invoice.init();
			Invoice.modal.on('click tap', '.submit-user-details', e => {
				e.preventDefault();
				e.stopPropagation();
				$('.loader-jl').appendTo(Invoice.modal).fadeIn();
				$.post(window.ajaxUrl, {
					action: 'invoice-user',
					user: Invoice.getUser(),
					ref: Invoice.varRef
				}, response => {
					_.each(JSON.parse(response), (o, k) => {
						$('.user-details [data-prop="' + k + '"]').text(o || '');
					});
					$('.user-details .billing')[JSON.parse(response)['billing_co'] ? 'removeClass': 'addClass']('hidden');
					Invoice.modal.modal('hide');
					$('.loader-jl').fadeOut(function(){
						$(this).appendTo('.jl-order-page')
					});
				});
			});
		} else {
			setTimeout(parseInvoice, 1000);
		}
	}
}(jQuery));
