(function ($) {
	let $_Cart, formatter = new Intl.NumberFormat('en-IE', {
		style: 'currency',
		currency: 'EUR',
		minimumFractionDigits: 2,
		}),
		VAT_RATE = .23;
	
	class Product {
		constructor(a) {
			_.extend(this, {
				ID: a[0],
				Manufacturer: a[1],
				ModelNo: a[2],
				Suffix: a[3],
				Display: a[4],
				Lamp_Qty: a[5],
				Manupartcode: a[6],
				Lamp_Supply: a[7],
				Lamphours: a[8],
				Wattage: a[9],
				Lamptype: a[10],
				Trade_Price: formatter.format(a[11] + $_Cart.tpp),
				Available_Stock: a[12],
				Qty_on_order: a[13],
				Typical_Leadtime: a[14],
				CanX: a[15]
			})
		}
	}
	
	class JL_Cart {
		constructor() {
			this.products = [];
			this.add = i => {
				let product = this.find('p-' + i[0]);
				if (product) {
					product.qty += 1;
				}
				else {
					this.products.push(i);
				}
				this.update();
			};
			this.controls = () => $('<div />', {
				class: 'space-between',
				html: '<span class="arrow_triangle-up"></span>' +
				'<span class="arrow_triangle-down"></span>' +
				'<span class="close">&times;</span>'
			});
			// this.count = () => this.products.reduce((a, b) => a + b['qty'], 0);
			this.tpp = 80;
			this.update = () => {
				let table = $('.checkout__summary'),
					tbody = $('tbody', table);
				tbody.html('');
				if (this.products.length) {
					$.each(this.products, (i, e) => {
						if (!e.qty) e.qty = 1;
					});
					$.each(this.products, (i, e) => {
						let tr = $('<tr />', {
							id: 'p-' + e[0]
						});
						tr.append($('<td />', {
							html: $('<div />', {
								class: 'space-between',
								html: '<span>' + e[2] + '</span><span>' + e[1] + '</span>'
							})
						})).append($('<td />', {
							text: e.qty
						})).append($('<td />', {
							text: formatter.format(e[11] + this.tpp)
						}))
							.append($('<td />', {
								html: this.controls(e)
							}));
						tbody.append(tr);
					})
				} else {
					tbody.append($('<tr />', {
						html: $('<td />', {
							colspan: 4,
							style: 'color:var(--color-2)',
							text: 'You haven\'t added any products'
						})
					}))
				}
				$('.checkout').addClass('checkout--active');
				$('.checkout__total').text(this.toLocale(this.total()));
				$('.checkout__vat').text(this.toLocale(this.total() * VAT_RATE));
				$('.checkout__grand').text(this.toLocale(this.total() * (1 + VAT_RATE)));
			};
			this.total = () => {
				if (this.products.length) {
					let result = 0;
					_.map(this.products, p => {
						result += Number(p[11] + this.tpp) * Number(p.qty || 1);
					});
					return result;
				}
				return 0;
			};
			this.toLocale = sum => formatter.format(sum);
			this.stock = (product, qty) => {
				product.qty = product.qty || 1;
				product.qty += qty;
				if (product.qty === 0) {
					this.remove(product);
				}
			};
			this.remove = item => _.remove(this.products, item);
			this.find = id => _.find(this.products, ['0', parseInt(id.substring(2))]);
			this.clear = () => {
				this.products = [];
				this.update();
			};
		}
	}
	
	window.sendRealexRequest = () => {
		if (window.rHpp) {
			const action_url = window.rHpp.action === 'live' ?
				'' :
				'https://pay.sandbox.realexpayments.com/pay';
			delete window.rHpp.action;
			let form = $('<form />', {
				id: 'realexForm',
				method: 'POST',
				action: action_url,
				style: 'visibility:hidden;opacity:0.01;height:0;position:absolute;overflow:hidden;'
			}), data = JSON.parse(window.rHpp);
			for (let prop in data) {
				let input = $('<input />', {
					type: 'hidden',
					value: data[prop],
					name: prop
				});
				form.append(input);
			}
			form.append($('<button />', {
				type: 'submit',
				value: data['CARD_PAYMENT_BUTTON']
			}));
			$('body').append(form);
			setTimeout(() => form.submit());
		}
	};
	
	window.render_projector = function (item) {
		let prod = new Product(_.values(item));
		$('#order-projector [data-prop]').each(
			(i, e) => $(e).text(prod[$(e).data('prop')])
		);
		$('#order-projector').css('opacity', 1);
		let checkout = '#realex_checkout';
		$(checkout + ' .qbutton').remove();
		let button = $('<a />', {
			class: 'qbutton large realex_payment',
			text: 'Add to cart',
			href: ''
		});
		$(checkout).append(button);
		button.on('click tap', e => {
			e.preventDefault();
			$_Cart.add(item);
			return false;
		});
	};
	$(window).on('load', function () {
		$_Cart = new JL_Cart();
		
		$('.checkout').each((i, el) => {
			let openCtrl = $('.checkout__button', el),
				closeCtrl = $('.checkout__cancel', el);
			openCtrl.on('click tap', e => {
				e.preventDefault();
				$(el).addClass('checkout--active')
			});
			closeCtrl.on('click tap', () => {
				$(el).removeClass('checkout--active')
			});
			
			$(el).closest('.full_section_inner').css({
				zIndex: 21
			});
			$(el).on('click tap', '.checkout__option--silent', () => {
				$('#jl-makers').val('').trigger('focus').trigger('search');
			}).on('click tap', 'tbody .space-between span.arrow_triangle-down', e => {
				let product = $_Cart.find($(e.target).closest('tr').attr('id'));
				$_Cart.stock(product, -1);
				$_Cart.update();
			}).on('click tap', 'tbody .space-between span.arrow_triangle-up', e => {
				let product = $_Cart.find($(e.target).closest('tr').attr('id'));
				$_Cart.stock(product, 1);
				$_Cart.update();
			}).on('click tap', 'tbody .space-between span.close', e => {
				let product = $_Cart.find($(e.target).closest('tr').attr('id'));
				$_Cart.remove(product);
				$_Cart.update();
			}).on('click tap', '.checkout__clear', e => {
				$_Cart.clear();
				$_Cart.update()
			}).on('click tap', '.sendRealexRequest', () => {
				if ($_Cart.products.length) {
					$('.loader-jl').appendTo('.checkout__order').fadeIn();
					$.post(window.ajaxUrl, {
						action: 'realex-payment',
						products: $_Cart.products
					}, response => {
						window.rHpp = response;
						$('.loader-jl').fadeOut();
						sendRealexRequest();
					});
				} else {
					alert('Your cart is empty.');
				}
			});
		});
		$('.chekout-bg').on('click tap', () => {
			$('.checkout').removeClass('checkout--active');
		});
	})
})(jQuery);

