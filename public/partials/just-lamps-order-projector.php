<?php
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 28.04.2018
	 * Time: 20:49
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );
?>
<div style="position:relative">
    <div class="loader-jl">
        <div class="loader-5 center"><span></span></div>
    </div>
    <div id="order-projector">
        <h3 class="product-title"><span data-prop="Manufacturer"></span> &mdash; <span
                    data-prop="ModelNo"></span>&nbsp;<span data-prop="Suffix"></span></h3>
        <div class="flex-wrap">
            <ul class="product-details">
                <li>
                    <strong>Lamps per model</strong>
                    <span data-prop="Lamp_Qty"></span>
                </li>
                <li>
                    <strong>Product code</strong>
                    <span data-prop="Manupartcode"></span>
                </li>
                <li>
                    <strong>Display</strong>
                    <span data-prop="Display"></span>
                </li>
                <li>
                    <strong>Lamp Hours</strong>
                    <span data-prop="Lamphours"></span>
                </li>
                <li>
                    <strong>Wattage</strong>
                    <span data-prop="Wattage"></span>
                </li>
                <li>
                    <strong>Typical Lead Time (Days)</strong>
                    <span data-prop="Typical_Leadtime"></span>
                </li>
                <li>
                    <strong>CanX</strong>
                    <span data-prop="CanX"></span>
                </li>
                <li>
                    <strong>Stock</strong>
                    <span data-prop="Available_Stock"></span>
                </li>
            </ul>
            <div class="order-flex" id="realex_checkout">
                <div data-prop="Trade_Price"></div>
                <div></div>
                <div class="qbutton large">Add to cart</div>
            </div>
        </div>
    </div>
</div>
