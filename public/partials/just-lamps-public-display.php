<?php

	/**
	 * Just-lamps form template.
	 *
	 * @link       http://websiter.ro
	 * @since      1.0.0
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/public/partials
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );
?>[vc_row]
    [vc_column]
<?php include "checkout.php"; ?>
    [/vc_column]
  [/vc_row]
  [vc_row]
    [vc_column width='1/2']
<?php echo do_shortcode( '[jlc-makers]' ); ?>
    [/vc_column]
    [vc_column width='1/2']
<?php echo do_shortcode( '[jlc-search]' ); ?>
    [/vc_column]
  [/vc_row]
  [vc_row]
    [vc_column]
<?php echo do_shortcode( '[projector-form]' ); ?>
    [/vc_column]
  [/vc_row]
<?php
