<?php
	/**
	 * The admin-specific functionality of the plugin.
	 *
	 * @link       http://websiter.ro
	 * @since      1.0.0
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/admin
	 */

	/**
	 * The admin-specific functionality of the plugin.
	 *
	 * Defines the plugin name, version, and two examples hooks for how to
	 * enqueue the admin-specific stylesheet and JavaScript.
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/admin
	 * @author     Andrei Gheorghiu <mail@websiter.ro>
	 *
	 * @property bool read_only
	 * @property string filename
	 * @property array data
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	class Just_Lamps_Admin {

		const NAMES = [
			"ID",
			"Manufacturer",
			"ModelNo",
			"Suffix",
			"Display",
			"Lamp_Qty",
			"Manupartcode",
			"Lamp_Supply",
			"Lamphours",
			"Wattage",
			"Lamptype",
			"Trade_Price",
			"Available_Stock",
			"Qty_on_order",
			"Typical_Leadtime",
			"Canx"
		];
		public $orders;
		/**
		 * Unique identifier
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string $plugin_name The ID of this plugin.
		 */
		private $plugin_name;
		/**
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string $version The current version of this plugin.
		 */
		private $version;
		/**
		 * Plugin option group
		 */
		private $option_group = 'just-lamps-group';
		private $settings;
		private $raw_data;
		private $read_only;
		private $filename;

		/**
		 * Init.
		 *
		 * @since    1.0.0
		 *
		 * @param      string $plugin_name The name of this plugin.
		 * @param      string $version The version of this plugin.
		 */
		public function __construct( $plugin_name, $version ) {

			$this->plugin_name = $plugin_name;
			$this->version     = $version;
			$this->settings    = $this->get_jlc_settings();
			$this->filename    = $this->get_option('filename') ?: $this->get_filename();
			$this->raw_data    = '';
			$this->read_only   = false;
		}

		/**
         * @since    1.0.0
         *
		 * @return string
		 */
		public function get_filename() {
			if ( ! ( $this->has_fresh_data() && $this->filename ) ) {
				if ( ! $this->read_only && $this->can_write() ) {
					$raw_data = file_get_contents( $this->remote_filename() );
					if ( $raw_data && file_put_contents( $this->filename, $raw_data ) ) {
						$this->settings['filename']   = $this->filename;
						$this->settings['updated_at'] = (string) time();
						update_option( 'jlc-settings', $this->settings );

						return $this->filename;
					}
				}

				return $this->remote_filename();
			}

			return $this->filename;
		}

		private function has_fresh_data() {
			return $this->get_option('updated_at') && ( intval( $this->get_option('updated_at') ) > intval( strtotime( '-30 minutes' ) ) );
		}

		/**
		 * @since    1.0.0
		 *
		 * @return bool
		 */
		private function can_write() {
			$dir = wp_get_upload_dir()['basedir'] . '/just-lamps';
			if ( wp_mkdir_p( $dir ) && file_put_contents( $dir . '/data.txt', '', FILE_APPEND ) !== false ) {
				$this->filename = $dir . '/data.txt';

				return true;
			}
			$this->read_only = true;

			return ! $this->read_only;
		}

		/**
		 * @since    1.0.0
		 *
		 * @return string
		 */
		private function remote_filename() {
			if ( $this->get_option('filename')) {
				return $this->get_option('filename');
			}
			$connection = ftp_connect( $this->get_option('url') );
			ftp_login( $connection, $this->get_option('username'), $this->get_option('password') );
			$list = ftp_nlist( $connection, "." );
			if ( isset( $list[0] ) ) {
				$this->settings['filename'] = "ftp://"
				                              . $this->get_option('username') . ":"
				                              . $this->get_option('password') . "@"
				                              . $this->get_option('url') . "/"
				                              . $list[0];

				return $this->get_option('filename');
			}

			return '';
		}

		/**
		 * Register css for admin.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_styles() {

			wp_enqueue_style( 'data-tables', plugin_dir_url( __DIR__ ) . 'vendor/DataTables/dt.css', array(), $this->version, 'all' );
			wp_enqueue_style( 'jquery-autocomplete', plugin_dir_url( __DIR__ ) . 'vendor/autocomplete/jquery.autocomplete.min.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/just-lamps-admin.css', array(), $this->version, 'all' );
			global $pagenow;

			if ( $pagenow === 'admin.php' && isset( $_GET['page'] ) ) {
				if ( $_GET['page'] === $this->plugin_name . '-orders' ) {
					wp_enqueue_style( 'zebra-tooltips', plugin_dir_url( __DIR__ ) . 'vendor/zebra_tooltips/zebra_tooltips.min.css' );
					wp_enqueue_style( 'webui-popover', plugin_dir_url( __DIR__ ) . 'vendor/webui-popover/jquery.webui-popover.min.css' );
				}
			}

		}

		/**
		 * Register js for admin.
		 *
		 * @since    1.0.0
		 */
		public function enqueue_scripts() {

			global $pagenow;

			if ( $pagenow === 'admin.php' && isset( $_GET['page'] ) ) {
				wp_register_script( 'data-tables', plugin_dir_url( __DIR__ ) . 'vendor/DataTables/datatables.min.js' );
				wp_register_script( 'lodash-custom', plugin_dir_url( __DIR__ ) . 'vendor/lodash/lodash.min.js' );
				wp_register_script( 'jquery-autocomplete', plugin_dir_url( __DIR__ ) . 'vendor/autocomplete/jquery.autocomplete.min.js', array( 'jquery' ) );
				wp_enqueue_script( 'webui-popover', plugin_dir_url( __DIR__ ) . 'vendor/webui-popover/jquery.webui-popover.min.js' );
				if ( $_GET['page'] === $this->plugin_name . '-orders' ) {
					wp_enqueue_script( 'wp-pointer' );
					wp_enqueue_script( 'zebra-tooltips', plugin_dir_url( __DIR__ ) . 'vendor/zebra_tooltips/zebra_tooltips.min.js', array( 'jquery' ) );
					wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/just-lamps-admin.js', array(
						'data-tables',
						'lodash-custom',
						'jquery-autocomplete',
						'zebra-tooltips',
						'webui-popover'
					), $this->version, false );
				} else {
					wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/just-lamps-admin.js', array(
						'data-tables',
						'lodash-custom',
						'jquery-autocomplete',
						'webui-popover'
					), $this->version, false );
				}
			}
		}

		/**
		 * Add the plugin menu item.
		 *
		 * @since    1.0.0
		 */
		public function add_options_page() {
			add_menu_page(
				'Just Lamps Options',
				'Just Lamps',
				'manage_options',
				$this->plugin_name,
				array( $this, 'render_options_page' ),
				'dashicons-list-view',
				25
			);

			add_submenu_page(
				'just-lamps',
				'Just Lamps Products',
				'Products',
				'manage_options',
				$this->plugin_name . '-products',
				array( $this, 'render_products_page' )
			);
			add_submenu_page(
				'just-lamps',
				'Just Lamps Settings',
				'Orders',
				'manage_options',
				$this->plugin_name . '-orders',
				array( $this, 'render_orders_page' )
			);

			$this->register_settings();
		}

		/**
		 * @since    1.0.0
		 */
		private function register_settings() {
			register_setting( $this->option_group, 'jlc-settings', array( $this, 'validate_settings' ) );
		}

		/**
		 * @param $options
		 *
		 * @since    1.0.0
		 *
		 * @return mixed
		 */
		public function validate_settings( $options ) {
			return $options;
		}

		/**
		 *
		 */
		public function render_orders_page() {
			$jlOrders = new Just_Lamps_Orders_List();
			$jlOrders->prepare_items();
			?>
            <div class="wrap">
                <div id="icon-users" class="icon32"></div>
                <h2>Just Lamps Orders</h2>
                <form id="just-lamp-list-orders-form" method="post">
				    <?php $jlOrders->display(); ?>
                </form>
            </div>
			<?php
		}

		/**
		 * @since    1.0.0
		 */
		public function render_products_page() {
			?>
            <div class="wrap">
                <h1>Just Lamps Products</h1>
                <table id="jl-table" class="table dataTable no-footer tablepress" style="width: 100%;">
                    <thead class="thead-dark">
                    <tr>
                        <th><span>ID</span></th>
                        <th>Manufacturer</th>
                        <th><span>No.</span></th>
                        <th>Suffix</th>
                        <th>Display</th>
                        <th><span>Qty</span></th>
                        <th>Manupartcode</th>
                        <th><span>Supply</span></th>
                        <th><span>Hours</span></th>
                        <th><span>Watts</span></th>
                        <th><span>Type</span></th>
                        <th><span>Price</span></th>
                        <th><span>Stock</span></th>
                        <th><span>Min</span></th>
                        <th><span>Leadtime</span></th>
                        <th>CanX</th>
                    </tr>
                    </thead>
                </table>
            </div>
			<?php
		}

		/**
		 * Render the admin page.
		 *
		 * @since    1.0.0
		 */

		public function render_options_page() {
			?>
            <div class="wrap jl-admin-page">
                <h1>Just Lamps Plugin</h1>
                <form method="post" action="options.php">
					<?php
						settings_fields( $this->option_group );
						do_settings_sections( $this->option_group );

						echo '<input type="hidden" name="jlc-settings[order_page]" value="' . $this->get_option('order_page') . '">';
					?>
                    <table class="form-table">
                        <tr valign="top">
                            <td colspan="2" class="no-padding">
                                <h4 class="divider">Just Lamps FTP account</h4>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[url]">URL</label></th>
                            <td><input type="text" name="jlc-settings[url]" id="jlc-settings[url]"
                                       value="<?php echo esc_attr( $this->get_option('url') ) ?: 'jlpartner.justlamps.net'; ?>"/>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[username]">Username</label></th>
                            <td><input type="text" name="jlc-settings[username]" id="jlc-settings[username]"
                                       value="<?php echo esc_attr( $this->get_option('username') ); ?>"/></td>
                        </tr>

                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[password]">Password</label></th>
                            <td><input type="password" name="jlc-settings[password]" id="jlc-settings[password]"
                                       value="<?php echo esc_attr( $this->get_option('password') ); ?>"/></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h4 class="divider">RealEx account</h4>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label>URL</label></th>
                            <td>
                                <div class="radio-setting">
                                    <label>
                                        <input type="radio" name="jlc-settings[realex_url]"
                                               value="live"
                                            <?php echo $this->get_option('realex_url') === 'live' ? 'checked' : '' ?>><span>Live account</span>
                                    </label>
                                    <label>
                                        <input type="radio" name="jlc-settings[realex_url]"
                                               value="sandbox" <?php echo $this->get_option('realex_url') === 'live' ? '' : 'checked' ?>><span>Sandbox</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[account_name]">Account name (<code>merchantId</code>)</label></th>
                            <td><input type="text" name="jlc-settings[account_name]" id="jlc-settings[account_name]"
                                       value="<?php echo $this->get_option('account_name') ? $this->get_option('account_name') : ''; ?>">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[shared]">Shared secret</label></th>
                            <td><input type="password" name="jlc-settings[shared]" id="jlc-settings[shared]"
                                       value="<?php echo $this->get_option('shared') ? $this->get_option('shared')  : 'secret'; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <h4 class="divider">Settings</h4>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[currency]">Currency</label></th>
                            <td><input type="text" name="jlc-settings[currency]" id="jlc-settings[currency]"
                                       value="<?php echo $this->get_option('currency') ? $this->get_option('currency') : 'EUR'; ?>">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[vat_rate]">VAT rate</label></th>
                            <td><input type="text" name="jlc-settings[vat_rate]" id="jlc-settings[vat_rate]"
                                       value="<?php echo $this->get_option('vat_rate') ? $this->get_option('vat_rate') : 23; ?>"
                                       style="text-align: right"
                                >%
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><label for="jlc-settings[admin_email]">Admin email</label></th>
                            <td><input type="text" name="jlc-settings[admin_email]" id="jlc-settings[admin_email]"
                                       value="<?php echo $this->get_option('admin_email') ?>"
                                >
                            </td>
                        </tr>

                    </table>
					<?php submit_button( 'Save settings' ); ?>

                </form>
            </div>
			<?php
		}

		/**
		 * @return mixed
         *
		 * @since    1.0.0
		 */
		private function get_jlc_settings() {
			$settings = get_option( 'jlc-settings' );
			if ( ! isset($settings['admin_email'])) {
				$settings['admin_email'] = get_bloginfo('admin_email');
				update_option( 'jlc-settings', $settings );
			}
			if ( ! isset( $settings['order_page'] ) ) {
				$page_id                = wp_insert_post( array(
					'post_title'   => 'Your order',
					'post_type'    => 'page',
					'post_name'    => 'order',
					'post_status'  => 'publish',
					'post_content' => '[jl-order-page]'
				) );
				$settings['order_page'] = $page_id;
				update_option( 'jlc-settings', $settings );
			}

			return $settings;
		}

		/**
		 * @param $option
		 *
		 * @since    1.0.0
		 *
		 * @return null
		 */
		public function get_option($option) {
		    return isset($this->settings[$option]) ? $this->settings[$option] : null;
        }

		/**
		 *
		 */
        public function just_lamps_json() {
			$q = isset( $_POST['query'] ) ? $_POST['query'] : [];
			$r = $this->get_data();

			if ( isset( $q['m'] ) ) {
				$r = array_filter( $r, function ( $lamp ) use ( $q ) {
					return $q['m'] == $lamp[1];
				} );
			}

			if ( isset( $q['s'] ) ) {
				$r = array_filter( $r, function ( $lamp ) use ( $q ) {
					return intval( $q['s'] ) == intval( $lamp[0] ) || 1;
				} );
			}
			usort($r, function($a, $b) {
				return strcmp($a[2].$a[3], $b[2].$b[3]);
			});
			echo json_encode( array_values( $r ) );
			die();
		}

		private function get_data() {
			if ( $this->has_fresh_data() && ! empty( $this->data ) ) {
				return $this->data;
			}
			foreach (
				str_getcsv( $this->get_raw_data(), '
' ) as $item
			) {
				$lamp         = json_decode( '[' . $item . ']' );
				$this->data[] = $lamp;
			}

			return $this->data;
		}

		/**
		 * @since    1.0.0
		 *
		 * @return bool|string
		 */
		private function get_raw_data() {
			return file_get_contents( $this->get_filename() );
		}

		/**
		 * @since    1.0.0
		 */
		public function render_makers() {
			echo '<label for="jl-makers">Manufacturer</label><input class="autocomplete-input" id="jl-makers" type="search" name="jl-maker" placeholder="Type manufacturer name">';
			$producers = json_encode( array_values( array_unique( array_map( function ( $o ) {
				return $o[1];
			}, $this->get_data() ) ) ) );
			$tyle      = '.ui-autocomplete{position:absolute;top:100%;left:0;z-index:1000;float:left;display:none;min-width:160px;padding:4px 0;margin:2px 0 0;list-style:none;background-color:#fff;border-color:#ccc;border-color:rgba(0,0,0,.2);border-style:solid;border-width:1px;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;-webkit-box-shadow:0 5px 10px rgba(0,0,0,.2);-moz-box-shadow:0 5px 10px rgba(0,0,0,.2);box-shadow:0 5px 10px rgba(0,0,0,.2);-webkit-background-clip:padding-box;-moz-background-clip:padding;background-clip:padding-box}.ui-autocomplete .ui-menu-item>a.ui-corner-all{display:block;padding:3px 15px;clear:both;font-weight:400;line-height:18px;color:#555;white-space:nowrap}.ui-autocomplete .ui-menu-item>a.ui-corner-all.ui-state-active,.ui-autocomplete .ui-menu-item>a.ui-corner-all.ui-state-hover{color:#fff;text-decoration:none;background-color:#08c;border-radius:0;-webkit-border-radius:0;-moz-border-radius:0;background-image:none}.ui-autocomplete{max-height:300px; overflow-y: auto;overflow-x:hidden;}.ui-autocomplete li:hover{cursor:pinter;} .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus{font-weight:normal;}.ui-helper-hidden-accessible{display:none;}';
			$ajaxUrl   = admin_url( 'admin-ajax.php' );
			echo "<style>{$tyle}</style
><script type='text/javascript'
>(function($){
	$(window).on('load', ()=>{
		let jlP = $('#jl-products'),
            resetJlForm = () => {
                $('#order-projector').css('opacity',.2);
                $('#realex_payment').css({
                    pointerEvents:'none'
                });
                delete window.rHpp
            },
            ldr = $('.loader-jl');
		window.ajaxUrl = '$ajaxUrl';
		
		jlP.on('search',() => {
			$('#order-projector [data-prop]').each(function(i,e) {
                $(e).text('');
            });
			resetJlForm();
		});
		$('.autocomplete-input').on('focus', function(){
			if (!$(this).val().length)
				$(this).keydown();
		});
		$('#jl-makers').autocomplete({
		    minLength: 0,
			source:{$producers},
			select: (el,ui) => {
			    ldr.fadeIn();
				$.post(window.ajaxUrl,{
						action:'just-lamps',
						query:{
							m:ui.item.value
						}
					},
					response => {
					    ldr.fadeOut();
						jlP.prop('disabled', false)
							.autocomplete({
								source:JSON.parse(response),
								focus: ( event, ui ) => {
									jlP.val( ui.item[2] + ' • ' +  ui.item[3]);
									return false;
								},
								select: ( event, ui ) => {
									jlP.val( ui.item[2] + ' • ' +  ui.item[3]);
									render_projector(ui.item);
									return false;
								},
								minLength:0
							}).autocomplete('instance')._renderItem = (ul, item) => $('<li />',{
								text:item[2] + ' • ' +  item[3],
								class:item[15]
							}).appendTo(ul);
						jlP.focus();
					})
			}
		})
			.on('search',() => {
				jlP.val('').prop('disabled' ,true);
				$('#order-projector [data-prop]').each(function(i,e) {
                    $(e).text('');
                });
				resetJlForm();
			});
	});
	setTimeout(() => {
		$('.loader-jl').fadeOut();
		$('.ui-autocomplete')
			.on('mousewheel DOMMouseScroll', e => e.stopPropagation(), false);
	});
})(jQuery);</script>";
		}

		/**
		 * @since    1.0.0
		 */
		public function render_search() {
			echo '<label for="jl-products">Product</label><input class="autocomplete-input" id="jl-products" disabled type="search" name="jl-product" placeholder="Select product">';
		}

	}
