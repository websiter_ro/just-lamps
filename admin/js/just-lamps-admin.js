(function ($) {
	'use strict';
	
	$(window).on('load', function () {
		$.post(ajaxurl, {
			action: 'just-lamps',
			data: {
				foo: 'bar'
			}
		}, function (response) {
			let result = JSON.parse(response),
				$jlt = $('#jl-table');
			if (Array.isArray(result) && result.reduce(o => o).length) {
				let $jlTable = $jlt.dataTable({
						data: result,
						processing: true,
						select: false,
						pageLength: 1337,
						paging: true,
						deferRender: true,
						scrollY: $(window).height() - 202,
						lengthChange: false,
						scroller: true,
						scrollX: true,
					}),
					makers = _.uniq(_.map(result, '1'));
				
				setTimeout(() => {
					let $AC = $('<input />', {
						class: 'form-control',
						id: 'jl-makers',
						type: 'search',
						name: 'maker',
						placeholder: 'TYPE MANUFACTURER NAME'
					});
					$('#jl-table_wrapper').prepend($AC);
					$AC.on('search', function () {
						if (!$(this).val()) {
							$jlTable.api().search('').draw();
						}
					}).autocomplete({
						lookup: makers,
						onSelect: function (selection) {
							$jlTable.api().search(selection.value).draw();
						}
					});
					$('input[type="search"]').addClass('form-control');
				})
			} else {
				$jlt.html('<i>Sorry, no data.</i>');
			}
		});
		if ($('.jlTooltip').is('td'))
			new $.Zebra_Tooltips($('.jlTooltip'));
		let ordersTable = $('table.just-lamps_page_just-lamps-orders');
		$('.jlOrder', ordersTable).each(function () {
			let content = $(this).data('pointer-content'),
				tAble = $('<table />', {
					class: 'jlOrderTable'
				}),
				tHead = $('<thead />'),
				tBody = $('<tbody />'),
				tFoot = $('<tfoot />'),
				appendRow = (parent, tag, item) => {
					let row = $('<tr />');
					_.each(item, html => row.append($('<' + tag + ' />', {
						html
					})));
					parent.append(row);
				};
			_.each(content, (o, k) => appendRow(k ? o[0] ? tBody : tFoot : tHead, k ? 'td' : 'th', o));
			tAble.append(tHead).append(tBody).append(tFoot);
			$(this).webuiPopover({
				title: 'Test',
				content: tAble[0].outerHTML
			})
		})
	})
})(jQuery);
