<?php

	/**
	 * The file that defines the core plugin class
	 *
	 * A class definition that includes attributes and functions used across both the
	 * public-facing side of the site and the admin area.
	 *
	 * @link       http://websiter.ro
	 * @since      1.0.0
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 */

	/**
	 * The core plugin class.
	 *
	 * This is used to define internationalization, admin-specific hooks, and
	 * public-facing site hooks.
	 *
	 * Also maintains the unique identifier of this plugin as well as the current
	 * version of the plugin.
	 *
	 * @since      1.0.0
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 * @author     Andrei Gheorghiu <mail@websiter.ro>
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	class Just_Lamps {

		static $instance;
		public $orders;
		public $admin;
		public $public;
		public $i18n;
		public $plugin_dir_url;
		/**
		 * The loader that's responsible for maintaining and registering all hooks that power
		 * the plugin.
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      Just_Lamps_Loader $loader Maintains and registers all hooks for the plugin.
		 */
		protected $loader;
		/**
		 * The unique identifier of this plugin.
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string $plugin_name The string used to uniquely identify this plugin.
		 */
		protected $plugin_name;
		/**
		 * The current version of the plugin.
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string $version The current version of the plugin.
		 */
		protected $version;

		/**
		 * Define the core functionality of the plugin.
		 *
		 * Set the plugin name and the plugin version that can be used throughout the plugin.
		 * Load the dependencies, define the locale, and set the hooks for the admin area and
		 * the public-facing side of the site.
		 *
		 * @since    1.0.0
		 */
		public function __construct() {
			if ( defined( 'JUST_LAMPS_VERSION' ) ) {
				$this->version = JUST_LAMPS_VERSION;
			} else {
				$this->version = '1.0.0';
			}
			$this->plugin_name = 'just-lamps';

			$this->load_dependencies();
			$this->set_locale();
			$this->define_admin_hooks();
			$this->define_public_hooks();

		}

		/**
		 * Load the required dependencies for this plugin.
		 *
		 * Include the following files that make up the plugin:
		 *
		 * - Just_Lamps_Loader. Orchestrates the hooks of the plugin.
		 * - Just_Lamps_i18n. Defines internationalization functionality.
		 * - Just_Lamps_Admin. Defines all hooks for the admin area.
		 * - Just_Lamps_Public. Defines all hooks for the public side of the site.
		 *
		 * Create an instance of the loader which will be used to register the hooks
		 * with WordPress.
		 *
		 * @since    1.0.0
		 * @access   private
		 */
		private function load_dependencies() {

			/**
			 * The class responsible for orchestrating the actions and filters of the
			 * core plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-just-lamps-loader.php';

			/**
			 * The class responsible for defining internationalization functionality
			 * of the plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-just-lamps-i18n.php';
			$this->i18n = new Just_Lamps_i18n();

			/**
			 * The class responsible for defining all actions that occur in the admin area.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-just-lamps-admin.php';
			$this->admin = new Just_Lamps_Admin( $this->get_plugin_name(), $this->get_version() );

			/**
			 * The class to manage orders table.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-just-lamps-orders-list.php';

			/**
			 * The class to manage orders table.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-just-lamps-orders.php';
			$this->orders = new JL_Orders();

			/**
			 * The class responsible for defining all actions that occur in the public-facing
			 * side of the site.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-just-lamps-public.php';
			$this->public = new Just_Lamps_Public( $this->get_plugin_name(), $this->get_version() );

			$this->loader = new Just_Lamps_Loader();

		}

		/**
		 * The name of the plugin used to uniquely identify it within the context of
		 * WordPress and to define internationalization functionality.
		 *
		 * @since     1.0.0
		 * @return    string    The name of the plugin.
		 */
		public function get_plugin_name() {
			return $this->plugin_name;
		}

		/**
		 * Retrieve the version number of the plugin.
		 *
		 * @since     1.0.0
		 * @return    string    The version number of the plugin.
		 */
		public function get_version() {
			return $this->version;
		}

		/**
		 * Define the locale for this plugin for internationalization.
		 *
		 * Uses the Just_Lamps_i18n class in order to set the domain and to register the hook
		 * with WordPress.
		 *
		 * @since    1.0.0
		 * @access   private
		 */
		private function set_locale() {

			$this->i18n = new Just_Lamps_i18n();

			$this->loader->add_action( 'plugins_loaded', $this->i18n, 'load_plugin_textdomain' );

		}

		/**
		 * Register all of the hooks related to the admin area functionality
		 * of the plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 */
		private function define_admin_hooks() {

			$this->loader->add_action( 'admin_menu', $this->admin, 'add_options_page' );
			$this->loader->add_action( 'wp_ajax_just-lamps', $this->admin, 'just_lamps_json' );
			$this->loader->add_action( 'wp_ajax_nopriv_just-lamps', $this->admin, 'just_lamps_json' );

			global $pagenow;

			if ( $pagenow === 'admin.php' && isset( $_GET['page'] ) ) {
				if ( strpos( $_GET['page'], $this->plugin_name ) !== false ) {
					$this->loader->add_action( 'admin_enqueue_scripts', $this->admin, 'enqueue_scripts' );
				}
				if ( strpos( $_GET['page'], $this->plugin_name ) !== false ) {
					$this->loader->add_action( 'admin_enqueue_scripts', $this->admin, 'enqueue_styles' );
				}
			}
		}

		/**
		 * Register all of the hooks related to the public-facing functionality
		 * of the plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 */
		private function define_public_hooks() {

			$this->loader->add_action( 'wp_enqueue_scripts', $this->public, 'enqueue_styles' );
			$this->loader->add_action( 'wp_enqueue_scripts', $this->public, 'enqueue_scripts', 99 );
			$this->loader->add_action( 'wp_ajax_just-lamps', $this->admin, 'just_lamps_json' );
			$this->loader->add_action( 'wp_ajax_realex-payment', $this->public, 'payment_json' );
			$this->loader->add_action( 'wp_ajax_nopriv_realex-payment', $this->public, 'payment_json' );
			$this->loader->add_action( 'wp_ajax_invoice-user', $this->public, 'invoice_user' );
			$this->loader->add_action( 'wp_ajax_nopriv_invoice-user', $this->public, 'invoice_user' );
			$this->loader->add_action( 'init', $this->public, 'add_orders_permalink' );
			add_shortcode( 'just-lamps', array( $this->public, 'render' ) );
			add_shortcode( 'jlc-makers', array( $this->admin, 'render_makers' ) );
			add_shortcode( 'jlc-search', array( $this->admin, 'render_search' ) );
			add_shortcode( 'projector-form', array( $this->public, 'projector_form' ) );
			add_shortcode( 'jl-order-page', array( $this->public, 'order_page' ) );
		}

		/** Singleton instance */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public function log($log) {
			if ( is_array( $log ) || is_object( $log ) ) {
				error_log( print_r( $log, true ) );
			} else {
				error_log( $log );
			}
		}

		/**
		 * Run the loader to execute all of the hooks with WordPress.
		 *
		 * @since    1.0.0
		 */
		public function run() {
			$this->loader->run();
		}

		/**
		 * The reference to the class that orchestrates the hooks with the plugin.
		 *
		 * @since     1.0.0
		 * @return    Just_Lamps_Loader    Orchestrates the hooks of the plugin.
		 */
		public function get_loader() {
			return $this->loader;
		}

	}
