<?php

	/**
	 * Fired during plugin deactivation
	 *
	 * @link       http://websiter.ro
	 * @since      1.0.0
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 */

	/**
	 * Fired during plugin deactivation.
	 *
	 * This class defines all code necessary to run during the plugin's deactivation.
	 *
	 * @since      1.0.0
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 * @author     Andrei Gheorghiu <mail@websiter.ro>
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	class Just_Lamps_Deactivator {

		/**
		 * Short Description. (use period)
		 *
		 * Long Description.
		 *
		 * @since    1.0.0
		 */
		public static function deactivate() {

		}

	}
