<?php
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 02.06.2018
	 * Time: 23:43
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	global $hook_suffix;

	if ( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-screen.php' );
		require_once( ABSPATH . 'wp-admin/includes/screen.php' );
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
		require_once( ABSPATH . 'wp-admin/includes/template.php' );
	}

	class Just_Lamps_Orders_List extends WP_List_Table {
		/**
		 * Prepare the items (pre render table page)
		 *
		 * @return Void
		 */
		public function prepare_items() {

			global $JL_PLUGIN;

			$columns  = $this->get_columns();
			$hidden   = $this->get_hidden_columns();
			$sortable = $this->get_sortable_columns();

			$this->_column_headers = array($columns, $hidden, $sortable);

			$this->process_bulk_actions();

			$data = $JL_PLUGIN->orders->get_all_data();

			$orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
			$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';

			function sort_data($a,$b) {
				$orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'id';
				$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'desc';
				$result = strcmp($a[$orderby], $b[$orderby]);
				return ($order==='asc') ? $result : -$result;
			}
			usort($data, 'sort_data');

			$per_page = 50;
			$current_page = $this->get_pagenum();
			$total_items = count($data);

			$this->items = $JL_PLUGIN->orders->get_data($per_page, $current_page, $orderby, $order);

			$this->set_pagination_args( array(
				'total_items' => $total_items,
				'per_page'    => $per_page,
				'total_pages' => ceil($total_items/$per_page)
			) );
		}

		/**
		 * Override the parent columns method. Defines the columns to use in your listing table
		 *
		 * @return array
		 */
		public function get_columns() {
			$columns = array(
				'cb'      => '<input type="checkbox" />',
				'id'      => 'ID',
				'date'    => 'RealEx <code>timestamp</code>',
				'price'   => 'Amount',
				'invoice' => 'Products',
				'address' => 'Address',
				'user'    => 'Client',
				'var_ref' => 'Ref.',
				'status'  => 'Status'
			);

			return $columns;
		}

		/**
		 * Define which columns are hidden
		 *
		 * @return array
		 */
		public function get_hidden_columns() {
			return array();
		}

		/**
		 * Define the sortable columns
		 *
		 * @return array
		 */
		public function get_sortable_columns() {
			return array( 'title' => array( 'title', false ) );
		}

		/**
		 * Define what data to show on each column of the table
		 *
		 * @param  array $item Data
		 * @param  String $column_name - Current column name
		 *
		 * @return Mixed
		 */
		public function column_default( $item, $column_name ) {
			switch ( $column_name ) {
				case 'id':
				case 'var_ref':
				case 'status':
					return $item[ $column_name ];
				default:
					return print_r( $item, true );
			}
		}

		/**
		 * @param $item
		 * @param $classes
		 * @param $data
		 * @param $primary
		 */
		public function _column_date( $item, $classes, $data, $primary ) {
			$classes    .= ' jlTooltip';
			$data       .= " data-zebra-tooltip='Created: " . $item['created_at'] .
			               ( $item['updated_at'] ? " \nUpdated: " . $item['updated_at'] : "" ) . "'";
			$attributes = "class='$classes' $data";
			echo "<td $attributes>";
			echo $item['timestamp'];
			echo $this->handle_row_actions( $item, 'date', $primary );
			echo "</td>";
		}

		/**
		 * @param $item
		 * @param $classes
		 * @param $data
		 * @param $primary
		 */
		public function _column_price( $item, $classes, $data, $primary ) {
			$classes    .= ' jlTooltip';
			$data       .= " data-zebra-tooltip='" . $item['comment1'] . "'";
			$attributes = "class='$classes' $data";
			echo "<td $attributes>";
			echo number_format( (float) $item['amount'] / 100, 2 );
			echo $this->handle_row_actions( $item, 'price', $primary );
			echo "</td>";
		}

		/**
		 * @param $item
		 * @param $classes
		 * @param $data
		 * @param $primary
		 */
		public function _column_invoice( $item, $classes, $data, $primary ) {
			$invoice_link = null;
			if ( $item['request'] ) {
				$uid          = json_decode( $item['request'] )->pas_uuid;
				$id           = $item['var_ref'];
				$invoice_link = '/order/' . $id . '/?uid=' . $uid;
			}
			echo "<td class='$classes' $data><a href='javascript:void(0);' class='jlOrder' data-pointer-content='{$item['invoice_table']}'>Details</a>" . ( $invoice_link ? " | <a href='{$invoice_link}' target='_blank'>View invoice page</a>" : "" );
			echo $this->handle_row_actions( $item, 'invoice', $primary );
			echo "</td>";
		}

		/**
		 * @param $item
		 * @param $classes
		 * @param $data
		 * @param $primary
		 */
		public function _column_user( $item, $classes, $data, $primary ) {
			$has_name = strlen( $item['user_name'] );
			$has_mail = strlen( $item['user_email'] );
			if ( $has_name && $has_mail ) {
				$classes .= ' jlTooltip';
				$data    .= " data-zebra-tooltip='" . $item['user_email'] . "'";
			}
			$attributes = "class='$classes' $data";
			echo "<td $attributes>";
			echo $has_name ? $item['user_name'] : $has_mail ? $item['user_email'] : '&mdash;';
			echo $this->handle_row_actions( $item, 'user', $primary );
			echo "</td>";
		}

		/**
		 * @param $item
		 * @param $classes
		 * @param $data
		 * @param $primary
		 */
		public function _column_address( $item, $classes, $data, $primary ) {
			$address = [];
			foreach ( [ 'shipping_co', 'shipping_code', 'billing_co', 'billing_code' ] as $a ) {
				if ( $item[ $a ] ) {
					$address[] = $item[ $a ];
				}
			}
			if ( count( $address ) ) {
				$classes .= ' jlTooltip';
				$data    .= " data-zebra-tooltip='" . implode( " \n", $address ) . "'";
			}
			$attributes = "class='$classes' $data";
			echo "<td $attributes>";
			echo count( $address ) ? 'Address' : '&mdash;';
			echo $this->handle_row_actions( $item, 'address', $primary );
			echo "</td>";
		}

		/**
		 * @param $item
		 * @param $classes
		 * @param $data
		 * @param $primary
		 */
		public function _column_status( $item, $classes, $data, $primary ) {
			$classes    .= " status-" . str_replace( ' ', '-', $item['status'] );
			$attributes = "class='$classes' $data";
			echo "<td $attributes>";
			echo $item['status'];
			echo $this->handle_row_actions( $item, 'status', $primary );
			echo "</td>";
		}

		function column_cb($item) {
			return sprintf(
				'<input type="checkbox" name="jl-orders[]" value="%s" />', $item['id']
			);
		}

		function get_bulk_actions() {
			$actions = array(
				'delete'    => 'Delete'/*,
				'export'    => 'Export'*/
			);
			return $actions;
		}

		public function process_bulk_actions() {

			if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {

				$nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
				$action = 'bulk-' . $this->_args['plural'];

				if ( ! wp_verify_nonce( $nonce, $action ) )
					wp_die( 'Nope! Security check failed!' );

			}

			$action = $this->current_action();

			switch ( $action ) {

				case 'delete':
					$ids = esc_sql( $_POST['jl-orders'] );
					foreach ( $ids as $id ) {
						self::delete_order( $id );
					}

					break;

				case 'export':
					$ids = esc_sql( $_POST['jl-orders'] );

					self::export_orders( $ids );

					wp_redirect( esc_url( add_query_arg() ) );
					break;

				default:
					// do nothing or something else
					return;
					break;
			}

			return;

		}

		private function delete_order($id) {
			global $JL_PLUGIN;
			$JL_PLUGIN->orders->delete_order($id);
		}

		private function export_orders($ids) {
			echo '<pre style="white-space: normal;word-break: break-all;">I should export orders ';
			print_r(json_encode($ids));
			echo ', but export function has not been coded yet.</pre>';
		}

	}
