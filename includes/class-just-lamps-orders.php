<?php
	/**
	 * Created by PhpStorm.
	 * User: andrei
	 * Date: 07.06.2018
	 * Time: 21:14
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	class JL_Orders {

		const TABLE_NAME = 'justlamps_orders';
		public $wp_list;

		// class constructor
		public function __construct() {
			if ( is_admin() ) {
				$this->wp_list = new Just_Lamps_Orders_List();
			}
		}

		public function get_all_data(){
			global $wpdb;
			$sql = "SELECT * FROM {$wpdb->prefix}" . self::TABLE_NAME;
			return $wpdb->get_results( $sql, 'ARRAY_A' );
		}

		/**
		 * @param int $per_page
		 * @param int $page_number
		 * @param $orderby
		 * @param $order
		 *
		 * @return array|null|object
		 */
			public function get_data( $per_page = 50, $page_number = 1, $orderby = 'id', $order = 'DESC') {
			global $wpdb;

			$sql = "SELECT * FROM {$wpdb->prefix}" . self::TABLE_NAME;

			$sql .= ' ORDER BY ' . esc_sql( $orderby );

			$sql .= ' ' . esc_sql( $order );

			$sql .= " LIMIT $per_page";

			$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


			$result = $wpdb->get_results( $sql, 'ARRAY_A' );

			return $result;

		}

		public function get_order( $id ) {
			global $wpdb;

			$sql = "SELECT * FROM {$wpdb->prefix}" . self::TABLE_NAME;
			$sql .= " WHERE id = $id;";

			$result = $wpdb->get_results( $sql, 'ARRAY_A' );

			return isset( $result[0] ) ? $result[0] : null;
		}

		public function get_order_by_code( $code ) {
			global $wpdb;

			$sql = "SELECT * FROM {$wpdb->prefix}" . self::TABLE_NAME;
			$sql .= " WHERE var_ref = {$code};";

			$result = $wpdb->get_results( $sql, 'ARRAY_A' );

			return $result[0];
		}

		public function get_status_message( $code ) {
			$codes = array(
				'0' => 'Paid',
				'1' => 'Not paid',
				'2' => 'Bank error',
				'3' => 'General error',
				'5' => 'Incorrect XML',
				'6' => 'Inactive account'
			);
			if ( isset( $code[0] ) ) {
				return $codes[ $code[0] ];
			} else {
				return 'Unknown error';
			}

		}

		public function update_order($data) {

			global $wpdb;

			$table = $wpdb->prefix . self::TABLE_NAME;

			$defaults = array(
				'id'            => null,
				'created_at'    => null,
				'updated_at'    => null,
				'timestamp'     => null,
				'amount'        => 0,
				'comment1'      => null,
				'comment2'      => null,
				'invoice_table' => null,
				'shipping_code' => null,
				'shipping_co'   => null,
				'billing_code'  => null,
				'billing_co'    => null,
				'user_name'     => null,
				'user_email'    => null,
				'user_phone'    => null,
				'var_ref'       => null,
				'status'        => 'in_progress',
				'request'       => null,
				'consent'       => 0,
				'email_sent'    => 0
			);
			$data     = wp_parse_args( $data, $defaults );

			$where = [ 'id' => $data['id'] ];

			$format = array(
				'%d',
				'%s',
				'%s',
				'%s',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d'
			);
			$result = $wpdb->update( $table, $data, $where, $format );

			return $result;
		}

		public function reset_orders() {
			$this->delete_all_orders();
		}

		public function delete_order($id) {
			global $wpdb;

			$result = $wpdb->delete( $wpdb->prefix . self::TABLE_NAME, array( 'id' => $id ) );
			if ($result === false) {
				error_log('Just Lamps Plugin failed while trying to delete order no. '.$id);
			}
		}

		private function delete_all_orders() {
				global $wpdb;

				$sql = "TRUNCATE TABLE {$wpdb->prefix}" . self::TABLE_NAME;

				return $wpdb->get_results( $sql, 'ARRAY_A' );

		}

	}
