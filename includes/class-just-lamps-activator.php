<?php

	/**
	 * Fired during plugin activation
	 *
	 * @link       http://websiter.ro
	 * @since      1.0.0
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 */

	/**
	 * Fired during plugin activation.
	 *
	 * This class defines all code necessary to run during the plugin's activation.
	 *
	 * @since      1.0.0
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 * @author     Andrei Gheorghiu <mail@websiter.ro>
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	class Just_Lamps_Activator {

		/**
		 * Short Description. (use period)
		 *
		 * Long Description.
		 *
		 * @since    1.0.0
		 */
		const JL_DB_VERSION = '1.0';
		const TABLE_NAME = 'justlamps_orders';
		private static $instance;

		public function __construct() {

			$db_version = null;//get_option('jl_db_version');
			if ( $db_version !== self::JL_DB_VERSION ) {
				require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
				if ( ! $db_version ) {
					$this->create_table();
				} else {
					$this->update_table();
				}
			}

		}

		private function create_table() {
			global $wpdb;

			$table_name      = $wpdb->prefix . self::TABLE_NAME;
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $table_name (
id mediumint(9) NOT NULL AUTO_INCREMENT,
created_at datetime DEFAULT CURRENT_TIMESTAMP,
updated_at datetime ON UPDATE NOW(),
timestamp varchar(15),
amount mediumint(9) NOT NULL,
comment1 varchar(255),
comment2 varchar(255),
invoice_table varchar(1023),
shipping_code varchar(31),
shipping_co varchar(255),
billing_code varchar(31),
billing_co varchar(255),
user_name varchar(127),
user_email varchar(127),
user_phone varchar(31),
var_ref varchar(255),
status varchar(31) DEFAULT 'in progress',
request text,
consent smallint(1),
email_sent smallint(1)
PRIMARY KEY (id)
) $charset_collate;";
			dbDelta( $sql );
		}

		private function update_table($sql) {
			global $wpdb;

			$table_name = $wpdb->prefix . self::TABLE_NAME;
			$sql        = "ALTER TABLE $table_name " . $sql;
			dbDelta( $sql );
			update_option( 'jl_db_version', self::JL_DB_VERSION );
		}

		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public static function update_columns() {
			global $wpdb;

			$table_name      = $wpdb->prefix . JL_Orders::TABLE_NAME;
			$existing_cols = $wpdb->get_col("DESC {$table_name}", 0);

			if (!in_array('consent', $existing_cols)) {
				$wpdb->query("ALTER TABLE {$table_name} ADD consent INT(1) NOT NULL DEFAULT 0");
			}

			if (!in_array('email_sent', $existing_cols)) {
				$wpdb->query("ALTER TABLE {$table_name} ADD email_sent INT(1) NOT NULL DEFAULT 0");
			}
		}
	}
