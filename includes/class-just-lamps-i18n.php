<?php

	/**
	 * Define the internationalization functionality
	 *
	 * Loads and defines the internationalization files for this plugin
	 * so that it is ready for translation.
	 *
	 * @link       http://websiter.ro
	 * @since      1.0.0
	 *
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 */

	/**
	 * Define the internationalization functionality.
	 *
	 * Loads and defines the internationalization files for this plugin
	 * so that it is ready for translation.
	 *
	 * @since      1.0.0
	 * @package    Just_Lamps
	 * @subpackage Just_Lamps/includes
	 * @author     Andrei Gheorghiu <mail@websiter.ro>
	 */
	defined( 'JUST_LAMPS_VERSION' ) or die( 'Meh... !?' );

	class Just_Lamps_i18n {


		/**
		 * Load the plugin text domain for translation.
		 *
		 * @since    1.0.0
		 */
		public function load_plugin_textdomain() {

			load_plugin_textdomain(
				'just-lamps',
				false,
				dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
			);

		}


	}
